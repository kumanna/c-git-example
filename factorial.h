#ifndef FACTORIAL_H
#define FACTORIAL_H

int
factorial(int);
double
stirling_approx(int n);

#endif
