#include "factorial.h"
#include <math.h>

int
factorial(int n)
{
        if (n < 0) return 0;
        if (n == 0) return 1;
        int product = 1;
        for (int i = 1; i <= n; ++i) {
                product = product * i;
        }
        return product;
}

double
stirling_approx(int n)
{
        return sqrt(2 * M_PI * n) * pow(n / M_E, n);
}
