all: main

main: main.o factorial.o
	$(CC) -o main main.o factorial.o -lm
main.o: main.c factorial.h
factorial.o: factorial.c factorial.h

.PHONY: clean
clean:
	$(RM) factorial.o main.o main
