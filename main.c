#include "factorial.h"
#include <stdio.h>

int
main(void)
{
	int i;
    printf("We will verify some factorials:\n");
	for (i = 0; i < 10; ++i) {
            printf("Factorial of %d is %d, Stirling: %f.\n", i, factorial(i), stirling_approx(i));
	}
	return 0;
}
